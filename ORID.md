Objective:
1.	Learn the concept of Agile Full Stack Developer
2.	Learn the process of software development
3.	Learn the learning arrangement of the following 4 weeks
4.	Learn and draw concept maps
5.	Learn ORID and do practices
Reflective:
1.	I am expected to be a full stack developer.
2.	I am confused that I don’t know how to do better in tasking and TDD.
3.	I am happy that I am going to have enrich days.
4.	I enjoy the collaboration with my team members when draw a concept map.
5.	I feel confused of how to make good use of URID to evaluate myself.
Interpretive:
1.	Because I love coding and I am eager to learn more programming techniques.
2.	I need to do more practice.
3.	Because I am more then eager to be a good OOCLer.
4.	Because I join in the activity and communicate with my teammates.
5.	Because I did too less practices.
Decisional:
1.	Study hard and finish my homework on time.
2.	Develop a software according to the process of software development I learned.
3.	Behave better in company and class.
4.	Be in harmony with my teammates and do better cooperation.
5.	Practice makes sense. I will do more practice as well as ask my teachers when I feel confused.
